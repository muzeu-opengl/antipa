#pragma once
#include <glfw/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

struct Camera {
    vec3 position;
    vec3 up;
    vec3 front;
    float pitch;
    float yaw;
    vec3 last_mouse_pos;
    bool first_mouse;
    float cam_speed;

    Camera() {
        position = vec3(0);
        up = vec3(0, 1, 0);
        front = vec3(0, 0, -1);
        yaw = 90;
        pitch = 0;
        cam_speed = 10;
        last_mouse_pos = vec3(0);
        first_mouse = true;
    }

    void Update(GLFWwindow* window, float dt)
    {
		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1)) {

			double mouseX, mouseY;
			glfwGetCursorPos(window, &mouseX, &mouseY);
			float deltaX = mouseX - last_mouse_pos.x;
			float deltaY = mouseY - last_mouse_pos.y;
			last_mouse_pos.x = mouseX;
			last_mouse_pos.y = mouseY;

			if (first_mouse) {
				first_mouse = false;
			}
			else {
				yaw += deltaX;
				pitch -= deltaY;

				// make sure that when pitch is out of bounds, screen doesn't get flipped
				if (pitch > 89.0f)
					pitch = 89.0f;
				if (pitch < -89.0f)
					pitch = -89.0f;
			}
            front.x = cos(radians(yaw)) * cos(radians(pitch));
            front.y = sin(radians(pitch));
            front.z = sin(radians(yaw)) * cos(radians(pitch));
            front = normalize(front);

            if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
                position -= front * cam_speed * dt;
            }
            if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
                position += cross(front, up) * cam_speed * dt;
            }
            if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
                position -= cross(front, up) * cam_speed * dt;
            }
            if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
                position += front * cam_speed * dt;
            }
        }
    }

    mat4 GetMatrix() 
    {
        mat4 view = lookAt(position, position + front, up);
        return view;
    }
};
