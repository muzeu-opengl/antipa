#version 330 core

out vec4 frag_color;

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

uniform sampler2D diffuse_texture;
uniform sampler2D shadow_texture;

uniform vec3 light_pos;
uniform vec3 cam_pos;
uniform Material material;

in vec3 frag_pos;
in vec3 normal;
in vec2 uv;
in vec4 frag_pos_light_space;

float CalculateShadow()
{
    // Perform perspective division
    vec3 proj_coords = frag_pos_light_space.xyz / frag_pos_light_space.w;
    // Map to [0,1]
    proj_coords = proj_coords * 0.5f + 0.5f;
    // Get closest depth value
    float closest_depth = texture(shadow_texture, proj_coords.xy).r;
    // Get current depth
    float current_depth = proj_coords.z;

    float bias = 0.005f;
    return current_depth - bias > closest_depth ? 1.0f : 0.0f;
}

void main() {

    vec3 color = (texture(diffuse_texture, uv)).rbg;
    vec3 diffuse = material.diffuse;
    vec3 specular = material.specular;
    vec3 ambient = material.ambient;

    vec3 light_dir = normalize(light_pos - frag_pos);

	float diff = max(dot(light_dir, normal), 0.0f);

	vec3 view_dir = normalize(cam_pos - frag_pos);
	vec3 reflected_dir = reflect(-light_dir, normal);

	float spec = pow(max(dot(view_dir, reflected_dir), 0.0f), material.shininess);

	diffuse *= diff;
	specular *= spec;

    float shadow = CalculateShadow();
    frag_color = vec4((ambient + (1.0f - shadow) * (diffuse + specular) * color), 1.0f);
}
