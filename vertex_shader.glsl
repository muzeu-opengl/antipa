#version 330 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_uv;
layout(location = 2) in vec3 in_normal;

out vec3 normal;
out vec2 uv;
out vec3 frag_pos;
out vec4 frag_pos_light_space;

uniform mat4 model;
uniform mat4 view_projection;
uniform mat4 light_space;
uniform mat4 normal_mat;

void main() {
    normal = normalize(mat3(normal_mat) * in_normal);
	uv = in_uv;

    // Model transformed position
	frag_pos = vec3(model * vec4(in_position, 1.0f));

    // Store light space transformed position 
    frag_pos_light_space = light_space * vec4(frag_pos, 1.0f);

	gl_Position = view_projection * model * vec4(in_position, 1.0f);
}

