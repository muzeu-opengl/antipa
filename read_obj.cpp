#include "object.h"
// OBJ Loading

struct OBJ_Info {
    int vertex_count;
    int face_count;
    int normal_count;
    int uv_count;
};

struct OBJ_Reader {
    char* file_str;
    int file_size;
    int idx;
};

struct OBJ_Face {
    int index_count;

    std::vector<int> position_indices;
    std::vector<int> uv_indices;
    std::vector<int> normal_indices;
};

// We assume here that line is not longer that 1024 chars
struct OBJ_Line {
    char str[1024];
    int idx;
};

// Parses the obj file to get the counts
OBJ_Info GetFileInfo(const char* filepath) {

    OBJ_Info obj_info = {0};

    FILE* fp = 0;
    fopen_s(&fp, filepath, "r");
    char line[256];

    if (fp == NULL) {
        printf("Failed to open obj file %s!\n", filepath);
    }

    while(fgets(line, 256, fp) != NULL) {

        if (line[0] == 'v') {
            obj_info.vertex_count += 1;
        } 
        else if (line[0] == 'v' && line[1] == 'n') {
            obj_info.normal_count += 1;
        } 
        else if (line[0] == 'v' && line[1] == 't') {
            obj_info.uv_count += 1;
        } 
        else if (line[0] == 'f') {
            obj_info.face_count += 1;
        }
    }

    fclose(fp);

    return obj_info;
}

int IsDigit(char c) {
    return (c >= '0' && c <= '9') || c == '.' || c == '-';
}

float GetFloat(OBJ_Line* line) {

    float result = 0.0f;
    char buffer[256];
    int idx = 0;

    // Copy string of the number to buffer
    while(IsDigit(line->str[line->idx])) {
        buffer[idx] = line->str[line->idx];
        idx++;
        line->idx++;
    }
    buffer[idx] = '\0';
    result = atof(buffer);

    return result;
}

int GetInt(OBJ_Line* line) {
    int result = -1;
    char buffer[256];
    int idx = 0;

    // No number
    if (line->str[line->idx] == '\\') {
        return 0;
    }

    // Copy string of the number to buffer
    while(IsDigit(line->str[line->idx])) {
        buffer[idx] = line->str[line->idx];
        idx++;
        line->idx++;
    }
    buffer[idx] = '\0';
    result = atoi(buffer);

    return result;
}

glm::vec3 Vec3FromLine(OBJ_Line* line) {
    glm::vec3 result;
    // Skip util digits start
    while (!IsDigit(line->str[line->idx]))
            line->idx++;

    for (int i = 0; i < 3; ++i) {
        result[i] = GetFloat(line);
        // Ship whitespace between numbers
        while(line->str[line->idx] != '\0' && 
              line->str[line->idx] == ' ') {
            line->idx++;
        }
    }
    return result;
}

glm::vec2 Vec2FromLine(OBJ_Line* line) {

    glm::vec2 result;
    // Skip util digits start
    while (!IsDigit(line->str[line->idx]))
            line->idx++;

    for (int i = 0; i < 2; ++i) {
        result[i] = GetFloat(line);
        // Ship whitespace between numbers
        while(line->str[line->idx] != '\0' && 
              line->str[line->idx] == ' ') {
            line->idx++;
        }
    }
    return result;
}

int NextLine(OBJ_Line* out, FILE* fp) {

    int success = 0;
    // Zero out everything
    out->idx = 0;
    for (int i = 0; i < sizeof(out->str); ++i) {
        out->str[0] = '\0';
    }

    if (fgets(out->str, sizeof(out->str), fp) != NULL) {
        success = 1;
    }

    return success;
}

OBJ_Face FaceFromLine(OBJ_Line* line) {

    // Face = position/uv/normal * index_count
    OBJ_Face face = {0};

    while(1) {

        if (IsDigit(line->str[line->idx])) {
            // Loop over position/uv
                // Subtract -1 since .obj is 1 indexed
            face.position_indices.push_back(GetInt(line) - 1);
            // Skip '/'
            line->idx++;
            face.uv_indices.push_back(GetInt(line) - 1);
            // Skip '/'
            line->idx++;
            face.normal_indices.push_back(GetInt(line) - 1);

            face.index_count++;
        }

        int do_break = 0;
        // Skip non digit
        while(!IsDigit(line->str[line->idx])) {
            if (line->str[line->idx] == '\0' ||
                line->str[line->idx] == '\n') {
                do_break = 1;
                break;
            }

            line->idx++;
        }
        if (do_break) {
            break;
        }
    }

    return face;
}

Object::Object() {
    diffuse_texture = TEXTURE_ID_NONE;
    vao = 0;
    vbo = 0;
    position = glm::vec3(0);
    scale = glm::vec3(1);
    rotation = glm::vec3(0);
    material.ambient = glm::vec3(0.1);
    material.diffuse = glm::vec3(0.5);
    material.specular = glm::vec3(0.4);
    material.shininess = 32.0f;
}

Object::Object(const char* filepath) : Object() {


    OBJ_Info obj_info = GetFileInfo(filepath);


    // Temporary buffers to store positions/uvs
    std::vector<glm::vec3> vertex_positions;
    std::vector<glm::vec2> uvs;
    std::vector<glm::vec3> normals;

    FILE* fp = 0;
    fopen_s(&fp, filepath, "r");

    OBJ_Line line = {0};
    while(NextLine(&line, fp) != 0) {

        if (line.str[0] == 'v' && line.str[1] == ' ') {
            vertex_positions.push_back(Vec3FromLine(&line));
        } 
        else if (line.str[0] == 'v' && line.str[1] == 't') {
            uvs.push_back(Vec2FromLine(&line));
        } 
        else if (line.str[0] == 'v' && line.str[1] == 'n') {
            normals.push_back(Vec3FromLine(&line));
        }
        else if (line.str[0] == 'f' && line.str[1] == ' ') {

            OBJ_Face face = FaceFromLine(&line);

            // If it is a triangle, no need to triangulate it
            if (face.index_count == 3) {
                for (int i = 0; i < face.index_count; ++i) {

                    Vertex vertex;
                    vertex.postition = vertex_positions[face.position_indices[i]];
                    vertex.uv = uvs[face.uv_indices[i]];
                    vertex.normal = normals[face.normal_indices[i]];

                    vertices.push_back(vertex);
                }
            }
            // Triangulate
            else {
                int triangle_count = face.index_count - 2;
                for (int offset = 0; offset < triangle_count; ++offset) {


                    Vertex vertex;
                    vertex.postition = vertex_positions[face.position_indices[0]];
                    vertex.uv = uvs[face.uv_indices[0]];
                    vertex.normal = normals[face.normal_indices[0]];

                    vertices.push_back(vertex);

                    for (int i = 1; i < 3; ++i) {

						vertex.postition = vertex_positions[face.position_indices[i + offset]];
						vertex.uv = uvs[face.uv_indices[i + offset]];
                        vertex.normal = normals[face.normal_indices[i + offset]];

						vertices.push_back(vertex);
                    }
                }
            }
        }
    }

    printf("Finised reading file  %s \n", filepath);
}



