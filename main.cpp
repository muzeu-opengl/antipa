
// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

// Include glad
#include <glad/glad.h>
#include <glad/glad.c>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>

// Include GLFW
#include <glfw/glfw3.h>

#include "shader.h"
#include "object.h"
#include "camera.h"

using namespace glm;

void ErrorCallback(int error, const char* description)
{
	printf("Glfw Error %d: %s\n", error, description);
}

void DrawScene(Shader* shader, vec3 light_pos, vec3 cam_pos, std::vector<Object> objects)
{
    for (int i = 0; i < objects.size(); ++i) {
        objects[i].Draw(shader);
    }
}


void RenderQuad()
{
	static unsigned int quadVAO = 0;
	static unsigned int quadVBO = 0;

	if (quadVAO == 0)
	{
		float quadVertices[] = {
			// positions        // texture Coords
			-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
			-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
			 1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
			 1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
		};
		// setup plane VAO
		glGenVertexArrays(1, &quadVAO);
		glGenBuffers(1, &quadVBO);
		glBindVertexArray(quadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	}

	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
}



int main() 
{
	// Initialise GLFW
	if (!glfwInit())
	{
		printf("Failed to initialize GLFW\n");
		return -1;
	}

	glfwSetErrorCallback(ErrorCallback);

	// Open a window and create its OpenGL context
	GLFWwindow* window = glfwCreateWindow(640, 480, "3D demo", NULL, NULL);
	if (window == NULL) {
		printf("Failed to open GLFW window.");
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		printf("Failed to initialize GLAD\n");
		return -1;
	}

	glfwSwapInterval(1); // Enable vsync
	//specify the size of the rendering window
	glViewport(0, 0, 640, 480);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);

    // Enable depth testing
	glEnable(GL_DEPTH_TEST);  
	//glDepthFunc(GL_LESS);  
    
    Shader normal_shader, shadow_shader, debug_shader;
    normal_shader.Load("vertex_shader.glsl", "fragment_shader.glsl");
    shadow_shader.Load("vertex_shadow.glsl", "fragment_shadow.glsl");
    debug_shader.Load("vertex_debug.glsl", "fragment_debug.glsl");

    std::vector<Object> objects(7);

    objects[0] = Object("models/t_rex.obj");
    objects[0].position = vec3(0, 0, -1);
    objects[0].scale = vec3(0.5);
    objects[0].Upload();
    objects[0].AddTexture("models/white.png");

    GLuint white_texture = objects[0].diffuse_texture;

    objects[1] = Object("models/room.obj");
    objects[1].position = vec3(0, 0, 0);
    objects[1].scale = vec3(2);
    objects[1].Upload();
    objects[1].AddTexture(white_texture);

    objects[2] = Object("models/sword.obj");
    objects[2].position = vec3(3.5, 0, 0);
    objects[2].scale = vec3(0.05);
    objects[2].AddTexture(white_texture);
    objects[2].Upload();

    objects[3] = Object("models/cup_statue.obj");
    objects[3].position = vec3(-3.5, 0, 0);
    objects[3].scale = vec3(0.01);
    objects[3].Upload();
    objects[3].AddTexture("models/cup.bmp");

    objects[4] = Object("models/eagle_statue.obj");
    objects[4].position = vec3(-7.5, 3, 0.5);
    objects[4].scale = vec3(0.03);
    objects[4].Upload();
    objects[4].AddTexture("models/eagle.jpg");

    objects[5] = Object("models/woman_statue.obj");
    objects[5].position = vec3(7.5, 0, 2);
    objects[5].scale = vec3(0.01);
    objects[5].Upload();
    objects[5].AddTexture("models/woman.jpg");

    objects[6] = Object("models/sculpture1.obj");
    objects[6].position = vec3(10, 0, 0);
    objects[6].scale = vec3(0.01);
    objects[6].Upload();
    objects[6].AddTexture("models/sculpture1.bmp");

    Camera camera;
    camera.position = vec3(1, 4, 2);

    vec3 light_pos = vec3(0, 5, 0);

    //Generate framebuffer to store depth information
    GLuint depth_fbo;
    glGenFramebuffers(1, &depth_fbo);

    const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

    GLuint shadow_texture;
    glGenTextures(1, &shadow_texture);
    glBindTexture(GL_TEXTURE_2D, shadow_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 
                 SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);  

    glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadow_texture, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);  

    float last_time = 0;
    float delta_time = 0;

	while (!glfwWindowShouldClose(window))
	{
        float current_time = glfwGetTime();
        delta_time = last_time - current_time;
        last_time = current_time;

        camera.Update(window, delta_time);

        glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
        glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo);
		glClear(GL_DEPTH_BUFFER_BIT);

        shadow_shader.Bind();

        mat4 projection = ortho(-10.0f, 10.0f, -10.0f, 10.0f, 0.1f, 10.0f);
        mat4 view = lookAt(light_pos, vec3(0), vec3(0, 1, 0));
        mat4 light_space = projection * view;

        shadow_shader.SetMat4("view_projection", light_space);

        for (int i = 0; i < objects.size(); ++i) {
            objects[i].Draw(&shadow_shader);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

#if 1
        glViewport(0, 0, 640, 480);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        normal_shader.Bind();

        projection = perspective(radians(90.0f), 640.0f / 480.0f, 0.1f, 1000.0f);
        view = camera.GetMatrix();
        mat4 view_proj = projection * view;

        normal_shader.SetMat4("light_space", light_space);
        normal_shader.SetMat4("view_projection", view_proj);
        normal_shader.SetVec3("light_pos", light_pos);

        for (int i = 0; i < objects.size(); ++i) {
            objects[i].Draw(&normal_shader, shadow_texture);
        }
#else
        // Draws shadow texture on a quad. this was used for debugging purpose
        // to determine if shadow map is working correctly
        //
		glViewport(0, 0, 640, 480);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        debug_shader.Bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, shadow_texture);
        RenderQuad();
#endif

		glfwPollEvents();
		glfwSwapBuffers(window);
	}
}
