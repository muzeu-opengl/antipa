#pragma once
#include <glad/glad.h>
#include <glm/glm.hpp>

struct Shader {
    GLuint ID;

    void Bind();
    void Load(const char* vertex_file_path, const char* fragment_file_path);

    GLint GetUniformLocation(const char* name);
    void SetMat4(const char* name, glm::mat4& matrix);
    void SetVec3(const char* name, glm::vec3& vec);
    void SetVec4(const char* name, glm::vec4& vec);
    void SetFloat(const char* name, float n);
    void SetInt(const char* name, int n);
};
