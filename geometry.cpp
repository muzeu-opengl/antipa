#include "object.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

#include <glm/gtc/matrix_transform.hpp>



void Object::Upload() {

	//Bindings for cubes
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	GLuint size = vertices.size() * sizeof(Vertex);
	glBufferData(GL_ARRAY_BUFFER, size, vertices.data(), GL_STATIC_DRAW);

	float stride = sizeof(Vertex);

	// Position
	glVertexAttribPointer(
		0,                  // attribute 0, must match the layout in the shader.
		3,                  // size of each attribute
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		stride,             // size of each vertex
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(0);

	// UV
	glVertexAttribPointer(
		1,         
		2,          
		GL_FLOAT,    
		GL_FALSE,     
		stride,
		(void*)sizeof(glm::vec3)
	);

	glEnableVertexAttribArray(1);

	// Normal
	glVertexAttribPointer(
		2,                
		3,               
		GL_FLOAT,       
		GL_FALSE,      
		stride,
		(void*)(sizeof(glm::vec3) + sizeof(glm::vec2))
	);

	glEnableVertexAttribArray(2);

	// has_texture
	glVertexAttribPointer(
		3,                
		1,               
		GL_FLOAT,       
		GL_FALSE,      
		stride,
		(void*)(sizeof(glm::vec3) + sizeof(glm::vec2) + sizeof(glm::vec3))
	);

	glEnableVertexAttribArray(3);

	//Unbinding VAO
	glBindVertexArray(0);
}

void Object::AddTexture(GLuint texture)
{
	diffuse_texture = texture;
}

void Object::AddTexture(const char* filepath) {

	int width, height, channels;
	stbi_set_flip_vertically_on_load(true);

    void* data = stbi_load(filepath, &width, &height, &channels, 0);

	if (data == NULL)
	{
		printf("Failed to load texture ( %s )!\n", filepath);
		return;
	}

    int slot = 0;
    glActiveTexture(GL_TEXTURE0 + slot);

    glGenTextures(1, &diffuse_texture);
    glBindTexture(GL_TEXTURE_2D, diffuse_texture);

	GLuint internal_format = 0;
	GLuint external_format = 0;

	if (channels == 4)
	{
		internal_format = GL_RGBA8;
		external_format = GL_RGBA;
	}
	else if (channels == 3)
	{
		internal_format = GL_RGB8;
		external_format = GL_RGB;
	}
	else if (channels == 1)
	{
		internal_format = GL_R8;
		external_format = GL_RED;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_RED);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_RED);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, GL_RED);
	}
	else
	{
		printf("Unsuported image format!\n");
		return;
	}

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
                 0, 
                 internal_format,
                 width,
                 height,
                 0,
                 external_format,
                 GL_UNSIGNED_BYTE, 
                 data);
}


void Object::Draw(Shader* shader, GLuint shadow_texture)
{
    shader->Bind();

    glm::mat4 model = glm::translate(glm::mat4(1), position) *
                      glm::rotate(glm::mat4(1), glm::radians(rotation.x), glm::vec3(1, 0, 0)) *
                      glm::rotate(glm::mat4(1), glm::radians(rotation.y), glm::vec3(0, 1, 0)) *
                      glm::rotate(glm::mat4(1), glm::radians(rotation.z), glm::vec3(0, 0, 1)) *
                      glm::scale(glm::mat4(1), scale);

    shader->SetMat4("model", model);

    // If it's a normal draw
    if (shadow_texture != TEXTURE_ID_NONE)
    {
        glm::mat4 normal_mat = transpose(inverse(model));
        shader->SetMat4("normal_mat", normal_mat);

        shader->SetVec3("material.ambient", material.ambient);
        shader->SetVec3("material.diffuse", material.diffuse);
        shader->SetVec3("material.specular", material.specular);
        shader->SetFloat("material.shininess", material.shininess);

        if (diffuse_texture != TEXTURE_ID_NONE) {
            shader->SetInt("diffuse_texture", 0);

            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, diffuse_texture);
        }

        shader->SetInt("shadow_texture",  1);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, shadow_texture);


    }
	glBindVertexArray(vao);

	glDrawArrays(GL_TRIANGLES, 0, vertices.size());
}
