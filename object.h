#pragma once

#include <glm/glm.hpp>
#include <glad/glad.h>
#include <vector>
#include "shader.h"

struct Vertex {
    glm::vec3 postition;
    glm::vec2 uv;
    glm::vec3 normal;
};

#define TEXTURE_ID_NONE 123456789

struct Material {
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    float shininess;
};

struct Object {
    std::vector<Vertex> vertices;
    GLuint vao, vbo;
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;

    Material material;

    GLuint diffuse_texture;

    Object();
    Object(const char* filepath);

    void AddTexture(const char* texture_path);
    void AddTexture(GLuint texture);
    void Upload();
    void Draw(Shader* shader, GLuint shadow_texture = TEXTURE_ID_NONE);
};


